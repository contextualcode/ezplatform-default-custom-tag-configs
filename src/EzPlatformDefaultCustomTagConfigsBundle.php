<?php

namespace ContextualCode\EzPlatformDefaultCustomTagConfigsBundle;

use ContextualCode\EzPlatformDefaultCustomTagConfigsBundle\DependencyInjection\Configuration\Parser\FieldType\RichText;
use eZ\Bundle\EzPublishCoreBundle\DependencyInjection\EzPublishCoreExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class EzPlatformDefaultCustomTagConfigsBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $this->registerConfigParser($container);
    }

    public function registerConfigParser(ContainerBuilder $container): void
    {
        $this->getCoreExtension($container)->addConfigParser(new RichText());
    }

    protected function getCoreExtension(ContainerBuilder $container): EzPublishCoreExtension
    {
        return $container->getExtension('ibexa');
    }
}
