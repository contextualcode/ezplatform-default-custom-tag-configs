<?php

declare(strict_types=1);

namespace ContextualCode\EzPlatformDefaultCustomTagConfigsBundle\DependencyInjection\Configuration\Parser\FieldType;

use Ibexa\Bundle\Core\DependencyInjection\Configuration\AbstractParser;
use Ibexa\Bundle\Core\DependencyInjection\Configuration\SiteAccessAware\ContextualizerInterface;
use Symfony\Component\Config\Definition\Builder\NodeBuilder;

class RichText extends AbstractParser
{
    private const TOOLBARS_NODE_KEY = 'toolbar';
    public const TOOLBARS_SA_SETTINGS_ID = 'fieldtypes.ezrichtext.' . self::TOOLBARS_NODE_KEY;
    public const CUSTOM_TAG_SETTINGS_ID = 'fieldtypes.ezrichtext.custom_tags';

    public const DEFAULT_TOOLBAR_CONFIG = [
        'buttons' => [
            'ezmoveup' => [
                'priority' => 80,
                'visible' => true,
            ],
            'ezmovedown' => [
                'priority' => 70,
                'visible' => true,
            ],
            'ezcustomtagedit' => [
                'priority' => 60,
                'visible' => true,
            ],
            'ezanchor' => [
                'priority' => 50,
                'visible' => true,
            ],
            'ezembedleft' => [
                'priority' => 40,
                'visible' => true,
            ],
            'ezembedcenter' => [
                'priority' => 30,
                'visible' => true,
            ],
            'ezembedright' => [
                'priority' => 20,
                'visible' => true,
            ],
            'ezblockremove' => [
                'priority' => 10,
                'visible' => true,
            ],
        ],
    ];

    /**
     * Returns the fieldType identifier the config parser works for.
     * This is to create the right configuration node under system.<siteaccess_name>.fieldtypes.
     *
     * @return string
     */
    public function getFieldTypeIdentifier(): string
    {
        return 'ezrichtext';
    }

    public function addSemanticConfig(NodeBuilder $nodeBuilder): void
    {
    }

    public function mapConfig(array &$scopeSettings, $currentScope, ContextualizerInterface $contextualizer): void
    {
        if (
            isset($scopeSettings[self::CUSTOM_TAG_SETTINGS_ID])
            && !empty($scopeSettings[self::CUSTOM_TAG_SETTINGS_ID])
        ) {
            if (!isset($scopeSettings[self::TOOLBARS_SA_SETTINGS_ID])) {
                $scopeSettings[self::TOOLBARS_SA_SETTINGS_ID] = [];
            }

            $customTags = $scopeSettings['fieldtypes.ezrichtext.custom_tags'] ?? [];
            $toolbars = $scopeSettings[self::TOOLBARS_SA_SETTINGS_ID] ?? [];

            if (!isset($scopeSettings[self::TOOLBARS_SA_SETTINGS_ID]['custom_tags_group']['buttons'])) {
                $scopeSettings[self::TOOLBARS_SA_SETTINGS_ID]['custom_tags_group']['buttons'] = [];
            }
            foreach ($customTags as $i => $customTag) {
                if (!isset($toolbars['custom_tags_group'][$customTag]) || empty($toolbars['custom_tags_group'][$customTag])) {
                    $scopeSettings[self::TOOLBARS_SA_SETTINGS_ID]['custom_tags_group'][$customTag] = self::DEFAULT_TOOLBAR_CONFIG;
                }
                if (!isset($scopeSettings[self::TOOLBARS_SA_SETTINGS_ID]['custom_tags_group']['buttons'])) {
                    $scopeSettings[self::TOOLBARS_SA_SETTINGS_ID]['custom_tags_group']['buttons'] = [];
                }
                $scopeSettings[self::TOOLBARS_SA_SETTINGS_ID]['custom_tags_group']['buttons'][$customTag] = [
                    'priority' => ($i + 1),
                    'visible' => true,
                ];
            }
        }
    }
}
